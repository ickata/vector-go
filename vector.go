package vector

type Vector struct {
	values [3]int
}

func NewVector(a, b, c int) Vector {
	values := [3]int{a, b, c}
	return Vector{values}
}

func (v Vector) Cross(vector Vector) Vector {
	return NewVector(
		v.values[1]*vector.values[2]-v.values[2]*vector.values[1],
		v.values[2]*vector.values[0]-v.values[0]*vector.values[2],
		v.values[0]*vector.values[1]-v.values[1]*vector.values[0])
}

func (v Vector) Dot(vector Vector) int {
	return v.values[0]*vector.values[0] + v.values[1]*vector.values[1] + v.values[2]*vector.values[2]
}
