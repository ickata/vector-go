# package vector

## Overview

Produces a new Vector struct

## Requirements

 - Go

## Run unit tests

`go test`

## Interface

### `Cross`

Returns cross product.

#### Parameters

1. `Vector`

#### Returns

`Vector`

### `Dot`

Returns dot product.

#### Parameters

1. `Vector`

#### Returns

`int`
