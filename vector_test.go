package vector

import "testing"

func TestCross(t *testing.T) {
	t.Run("Cross", func(t *testing.T) {
		v1 := NewVector(1, 1, 1)
		v2 := NewVector(2, 2, 2)

		v3 := v1.Cross(v2)
		values := [3]int{0, 0, 0}

		if v3.values != values {
			t.Errorf("Cross incorrect, got: %v, want: %v.", v3.values, values)
		}
	})
}

func TestDot(t *testing.T) {
	t.Run("Dot", func(t *testing.T) {
		v1 := NewVector(1, 1, 1)
		v2 := NewVector(2, 2, 2)

		result := v1.Dot(v2)

		if result != 6 {
			t.Errorf("Dot incorrect, got: %d, want: %d.", result, 6)
		}
	})
}
